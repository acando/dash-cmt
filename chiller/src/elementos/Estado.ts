import { stylesFactory } from '@grafana/ui';
import { css } from 'emotion';

const getStyles = stylesFactory(() => {
  return {
    alarmaOn: css`
      fill: #f51628;
    `,
    alarmaOff: css`
      fill: #4d4d4d;
    `,
    estadoOn: css`
      fill: #1aea78;
    `,
    estadoOff: css`
      fill: #4d4d4d;
    `,
    rectOn: css`
      fill: #4bee8e;
    `,
    rectOff: css`
      fill: gray;
    `,
    butOn: css`
      fill: #4bee8e;
    `,
    butOff: css`
      fill: #d10818;
    `,
  };
});

const equipo = getStyles();

export default equipo;
