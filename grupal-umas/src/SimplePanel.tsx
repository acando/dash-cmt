import React from 'react';
import { PanelProps } from '@grafana/data';
import { SimpleOptions } from 'types';
import { css, cx } from 'emotion';
//import { stylesFactory, useTheme } from '@grafana/ui';
import { stylesFactory } from '@grafana/ui';

interface Props extends PanelProps<SimpleOptions> {}

export const SimplePanel: React.FC<Props> = ({ options, data, width, height }) => {
  //const theme = useTheme();

  console.log(data);
  //CAMBIO NOMBRE EQUIPO
  let data_repla: any = data.series.find(({ refId }) => refId === 'B')?.name;
  let nombre: any = data_repla.replace(/[.*+?^${}()|[\]\\]/g, '');
  console.log(data_repla, nombre);
  let led_alm = '#f51628';
  let led_off = '#4d4d4d';
  let led_on = '#1aea78';
  let url = '';

  //INGRESO VARIABLES
  let tsum = data.series.find(({ name }) => name === 'Average DATA.DELI_AIR_TEMP.VALUE')?.fields[1].state?.calcs
    ?.lastNotNull;
  let tret = data.series.find(({ name }) => name === 'Average DATA.ROOM_TEMP.VALUE')?.fields[1].state?.calcs
    ?.lastNotNull;
  let rel_hum = data.series.find(({ name }) => name === 'Average DATA.ROOM_REL_HUM.VALUE')?.fields[1].state?.calcs
    ?.lastNotNull;
  let cold_valv = data.series.find(({ name }) => name === 'Average DATA.COLD_WAT_VALV.VALUE')?.fields[1].state?.calcs
    ?.lastNotNull;
  let run_hours = data.series.find(({ name }) => name === 'Average DATA.UNIT_RUN_ALARM.VALUE')?.fields[1].state?.calcs
    ?.lastNotNull;
  //let unit_alarm = data.series.find(({ name }) => name === 'Average DATA.UNIT_ALARM.VALUE')?.fields[1].state?.calcs?.lastNotNull;
  let troom = data.series.find(({ name }) => name === 'Average DATA.AVG_ROOM_TEMP.VALUE')?.fields[1].state?.calcs
    ?.lastNotNull;
  let st = data.series.find(({ name }) => name === 'Average DATA.SYS_ON.VALUE')?.fields[1].state?.calcs?.lastNotNull;
  let st2 = led_off;
  //let alm = led_off;
  let mnt = led_off;
  let st_valv = 'OFF';
  let st_fan = 'OFF';

  //PARPADEO ALARMA

  let alm_uma: any = revisar_data_status(
    data.series.find(({ name }) => name === 'Average DATA.UNIT_ALARM.VALUE')?.fields[1].state?.calcs?.lastNotNull
  );

  //audio carga de audios para alarmas
  function reproducir() {
    const audio = new Audio('/public/sound/alarm_warning.mp3');
    audio.play();
  }

  function revisar_data_status(stringdata: any) {
    let data_ret_string = [];
    //let st2fill='#ff9e2c';
    if (stringdata === null || stringdata === 0) {
      data_ret_string[0] = led_off; //color rojo
      data_ret_string[1] = '1s';
      //reproducir();
    } else {
      data_ret_string[0] = led_alm; //color rojo
      data_ret_string[1] = '1s';
      reproducir();
    }
    return data_ret_string;
  }

  //PROGRAMACION HIPERVINCULOS
  switch (nombre) {
    case '2/UMA-1':
      url = 'http://bmscloud.i.telconet.net:32308/d/2VQ6kkuMz/uma?orgId=1&var-nombre=2%2FUMA-1';
      break;
    case '2/UMA-2':
      url = 'http://bmscloud.i.telconet.net:32308/d/2VQ6kkuMz/uma?orgId=1&var-nombre=2%2FUMA-2';
      break;
    case '2/UMA-3':
      url = 'http://bmscloud.i.telconet.net:32308/d/2VQ6kkuMz/uma?orgId=1&var-nombre=2%2FUMA-3';
      break;
    case '2/UMA-4':
      url = 'http://bmscloud.i.telconet.net:32308/d/2VQ6kkuMz/uma?orgId=1&var-nombre=2%2FUMA-4';
      break;
    case '2/UMA-5':
      url = 'http://bmscloud.i.telconet.net:32308/d/2VQ6kkuMz/uma?orgId=1&var-nombre=2%2FUMA-5';
      break;
    case '2/UMA-6':
      url = 'http://bmscloud.i.telconet.net:32308/d/2VQ6kkuMz/uma?orgId=1&var-nombre=2%2FUMA-6';
      break;
    case '2/UMA-7':
      url = 'http://bmscloud.i.telconet.net:32308/d/2VQ6kkuMz/uma?orgId=1&var-nombre=2%2FUMA-7';
      break;
    case '2/UMA-8':
      url = 'http://bmscloud.i.telconet.net:32308/d/2VQ6kkuMz/uma?orgId=1&var-nombre=2%2FUMA-8';
      break;
    case '2/UMA-9':
      url = 'http://bmscloud.i.telconet.net:32308/d/2VQ6kkuMz/uma?orgId=1&var-nombre=2%2FUMA-9';
      break;
    case '2/UMA-10':
      url = 'http://bmscloud.i.telconet.net:32308/d/2VQ6kkuMz/uma?orgId=1&var-nombre=2%2FUMA-10';
      break;
    default:
      url = 'http://bmscloud.i.telconet.net:32308/d/2VQ6kkuMz/uma?orgId=1&var-nombre=2%2FUMA-1';
  }

  if (st === null || st === 0) {
    st = 0;
  } else {
    st = 1;
    st2 = led_on;
    st_fan = 'ON';
  }
  if (tsum === null || tsum === 0) {
    tsum = 0;
  } else {
    tsum = (tsum / 10).toFixed(1);
  }
  if (tret === null || tret === 0) {
    tret = 0;
  } else {
    tret = (tret / 10).toFixed(1);
  }
  if (rel_hum === null || rel_hum === 0) {
    rel_hum = 0;
  } else {
    rel_hum = (rel_hum / 10).toFixed(1);
  }
  if (cold_valv === null || cold_valv === 0) {
    cold_valv = 0;
  } else {
    cold_valv = (cold_valv / 10).toFixed(1);
    st_valv = 'ON';
  }
  if (troom === null || troom === 0) {
    troom = 0;
  } else {
    troom = (troom / 10).toFixed(1);
  }
  if (run_hours === null || run_hours === 0) {
    run_hours = 0;
  } else {
    run_hours = (run_hours / 10).toFixed(0);
  }
  //if (unit_alarm === null || unit_alarm === 0) {
  //unit_alarm = 0;
  //} else {
  //unit_alarm = 1;
  //alm = led_alm;
  //}

  const styles = getStyles();
  return (
    <div
      className={cx(
        styles.wrapper,
        css`
          width: ${width}px;
          height: ${height}px;
        `
      )}
    >
      <svg
        width={'100%'}
        height={'100%'}
        viewBox="0 0 457.72917 343.95835"
        id="svg10606"
        ///{...props}
      >
        <defs id="defs10600">
          <linearGradient id="linearGradient2725">
            <stop offset={0} id="stop2721" stopColor="#fff" stopOpacity={1} />
            <stop offset={1} id="stop2723" stopColor="#fff" stopOpacity={0} />
          </linearGradient>
          <radialGradient
            xlinkHref="#linearGradient2725"
            id="radialGradient2179-9"
            cx={65.833572}
            cy={208.85938}
            fx={65.833572}
            fy={208.85938}
            r={2.3812499}
            gradientTransform="matrix(7.56133 0 0 146.50901 -257.236 -29802.643)"
            gradientUnits="userSpaceOnUse"
          />
          <radialGradient
            xlinkHref="#linearGradient2725"
            id="radialGradient2727-1"
            cx={117.9915}
            cy={133.50375}
            fx={117.9915}
            fy={133.50375}
            r={16.463802}
            gradientTransform="matrix(3.89196 0 0 .18378 -17.435 485.845)"
            gradientUnits="userSpaceOnUse"
          />
          <filter
            id="filter4280-2"
            x={-0.192}
            width={1.3839999}
            y={-0.192}
            height={1.3839999}
            colorInterpolationFilters="sRGB"
          >
            <feGaussianBlur stdDeviation={1.915788} id="feGaussianBlur4282-2" />
          </filter>
          <filter
            id="filter4280-8-0"
            x={-0.192}
            width={1.3839999}
            y={-0.192}
            height={1.3839999}
            colorInterpolationFilters="sRGB"
          >
            <feGaussianBlur stdDeviation={1.915788} id="feGaussianBlur4282-7-8" />
          </filter>
          <filter
            id="filter4280-5-7"
            x={-0.192}
            width={1.3839999}
            y={-0.192}
            height={1.3839999}
            colorInterpolationFilters="sRGB"
          >
            <feGaussianBlur stdDeviation={1.915788} id="feGaussianBlur4282-8-6" />
          </filter>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath1199-50-1-6-6">
            <path
              d="M208.07 252.36l1.336-2.13 1.737-.833 4.944-.649 91.54.37 3.341.186 1.871.556 2.138 1.296 2.806 12.315-114.257-.37z"
              id="path1201-8-5-7-0"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath1199-5-1-4-3-6" clipPathUnits="userSpaceOnUse">
            <path
              id="path1201-4-2-2-1-3"
              d="M208.07 252.36l1.336-2.13 1.737-.833 4.944-.649 91.54.37 3.341.186 1.871.556 2.138 1.296 2.806 12.315-114.257-.37z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath1199-5-3-3-8-6-3">
            <path
              d="M208.07 252.36l1.336-2.13 1.737-.833 4.944-.649 91.54.37 3.341.186 1.871.556 2.138 1.296 2.806 12.315-114.257-.37z"
              id="path1201-4-0-4-5-4-2"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath1199-50-8-8" clipPathUnits="userSpaceOnUse">
            <path
              id="path1201-8-53-7"
              d="M208.07 252.36l1.336-2.13 1.737-.833 4.944-.649 91.54.37 3.341.186 1.871.556 2.138 1.296 2.806 12.315-114.257-.37z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath1199-5-1-1-4">
            <path
              d="M208.07 252.36l1.336-2.13 1.737-.833 4.944-.649 91.54.37 3.341.186 1.871.556 2.138 1.296 2.806 12.315-114.257-.37z"
              id="path1201-4-2-9-0"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
          <clipPath id="clipPath1199-5-3-3-0-9" clipPathUnits="userSpaceOnUse">
            <path
              id="path1201-4-0-4-4-8"
              d="M208.07 252.36l1.336-2.13 1.737-.833 4.944-.649 91.54.37 3.341.186 1.871.556 2.138 1.296 2.806 12.315-114.257-.37z"
              fill="none"
              stroke="#fff"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </clipPath>
        </defs>
        <g id="layer1">
          <g id="g336-4" transform="matrix(0 2.47939 -3.4361 0 350.511 124.424)" display="inline">
            <path
              d="M-17.773 99.38l1.42 1.892h2.177l.338-.78h66.963l.428.57h2.27l1.171-1.559H55.86l-.153.353H-16.37l-.23-.53z"
              id="path3295-6-7-6"
              display="inline"
              fill="#168198"
              fillOpacity={1}
              stroke="#3fdce4"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </g>
          <path
            d="M40.187 116.431V52.125h32.339v64.853z"
            id="path1391-9-8-18"
            display="inline"
            opacity={0.5}
            fill="#377e96"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.461814}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path1391-9-8-2-9"
            d="M39.017 203.325V139.02h32.34v64.852z"
            display="inline"
            opacity={0.5}
            fill="#377e96"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.461814}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path1391-9-8-9-8"
            d="M41.789 291.948v-64.305h32.339v64.852z"
            display="inline"
            opacity={0.5}
            fill="#377e96"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.461814}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path1391-9-8-1-4"
            d="M389.45 115.733V51.427h32.34v64.852z"
            display="inline"
            opacity={0.5}
            fill="#377e96"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.461814}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path1391-9-8-6-8"
            d="M389.846 203.84v-64.305h32.34v64.852z"
            display="inline"
            opacity={0.5}
            fill="#377e96"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.461814}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path1391-9-8-5-1"
            d="M387.867 290.403v-64.306h32.339v64.852z"
            display="inline"
            opacity={0.5}
            fill="#377e96"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.461814}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M105.775 72.391l252.134-1.275 16.8 23.734-.2 180.238-15.641 26.17-252.96 1.288-16.234-28.231.197-179.905z"
            id="rect1220-3"
            display="inline"
            fill="none"
            fillOpacity={0.5}
            fillRule="evenodd"
            stroke="#3fdbe4"
            strokeWidth={1.01983}
            strokeLinecap="square"
            strokeLinejoin="bevel"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="markers stroke fill"
          />
          <text
            transform="scale(.9331 1.0717)"
            xmlSpace="preserve"
            style={{
              lineHeight: '125%',
              ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontFeatureSettings: 'normal',
              textAlign: 'start',
            }}
            x={427.69427}
            y={81.494659}
            id="text6404-7-9-4"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="25.0154px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            writingMode="lr-tb"
            textAnchor="start"
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.644636}
            strokeMiterlimit={4}
            strokeDasharray="none"
          >
            <tspan
              style={{
                ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontFeatureSettings: 'normal',
                textAlign: 'start',
              }}
              x={427.69427}
              y={81.494659}
              id="tspan6402-7-2-4"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="25.0154px"
              fontFamily="Franklin Gothic Medium"
              writingMode="lr-tb"
              textAnchor="start"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.644636}
              strokeMiterlimit={4}
              strokeDasharray="none"
            >
              <tspan
                dx="0 0"
                dy="0 0"
                style={{
                  ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
                  fontVariantLigatures: 'normal',
                  fontVariantCaps: 'normal',
                  fontVariantNumeric: 'normal',
                  fontFeatureSettings: 'normal',
                  textAlign: 'start',
                }}
                id="tspan6398-8-2-1"
                fontStyle="normal"
                fontVariant="normal"
                fontWeight={400}
                fontStretch="normal"
                fontSize="25.0154px"
                fontFamily="Franklin Gothic Medium"
                writingMode="lr-tb"
                textAnchor="start"
                fill="#fff"
                fillOpacity={1}
                strokeWidth={0.644636}
                strokeMiterlimit={4}
                strokeDasharray="none"
              >
                {'\u2191'}
              </tspan>
              <tspan
                dx={0}
                dy={0}
                style={{
                  ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
                  fontVariantLigatures: 'normal',
                  fontVariantCaps: 'normal',
                  fontVariantNumeric: 'normal',
                  fontFeatureSettings: 'normal',
                  textAlign: 'start',
                }}
                id="tspan6400-8-8-4"
                fontStyle="normal"
                fontVariant="normal"
                fontWeight={400}
                fontStretch="normal"
                fontSize="25.0154px"
                fontFamily="Franklin Gothic Medium"
                writingMode="lr-tb"
                textAnchor="start"
                fill="#fff"
                fillOpacity={1}
                strokeWidth={0.644636}
                strokeMiterlimit={4}
                strokeDasharray="none"
              />
            </tspan>
          </text>
          <text
            transform="scale(.7441 1.34392)"
            xmlSpace="preserve"
            style={{
              lineHeight: '125%',
              ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontFeatureSettings: 'normal',
              textAlign: 'start',
            }}
            x={532.61597}
            y={134.74011}
            id="text6448-3-2-4"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="27.1617px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            writingMode="lr-tb"
            textAnchor="start"
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={1.85192}
          >
            <tspan
              style={{
                ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontFeatureSettings: 'normal',
                textAlign: 'start',
              }}
              x={532.61597}
              y={134.74011}
              id="tspan6446-1-3-0"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="27.1617px"
              fontFamily="Franklin Gothic Medium"
              writingMode="lr-tb"
              textAnchor="start"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={1.85192}
            >
              <tspan
                dx="0 0"
                dy="0 0"
                style={{
                  ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
                  fontVariantLigatures: 'normal',
                  fontVariantCaps: 'normal',
                  fontVariantNumeric: 'normal',
                  fontFeatureSettings: 'normal',
                  textAlign: 'start',
                }}
                id="tspan6442-6-4-4"
                fontStyle="normal"
                fontVariant="normal"
                fontWeight={400}
                fontStretch="normal"
                fontSize="27.1617px"
                fontFamily="Franklin Gothic Medium"
                writingMode="lr-tb"
                textAnchor="start"
                fill="#fff"
                fillOpacity={1}
                strokeWidth={1.85192}
              >
                {'\u2190'}
              </tspan>
              <tspan
                dx={0}
                dy={0}
                style={{
                  ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
                  fontVariantLigatures: 'normal',
                  fontVariantCaps: 'normal',
                  fontVariantNumeric: 'normal',
                  fontFeatureSettings: 'normal',
                  textAlign: 'start',
                }}
                id="tspan6444-3-4-2"
                fontStyle="normal"
                fontVariant="normal"
                fontWeight={400}
                fontStretch="normal"
                fontSize="27.1617px"
                fontFamily="Franklin Gothic Medium"
                writingMode="lr-tb"
                textAnchor="start"
                fill="#fff"
                fillOpacity={1}
                strokeWidth={1.85192}
              />
            </tspan>
          </text>
          <text
            transform="scale(1.0105 .9896)"
            xmlSpace="preserve"
            style={{
              lineHeight: '125%',
              ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontFeatureSettings: 'normal',
              textAlign: 'start',
            }}
            x={393.69962}
            y={267.75983}
            id="text6492-8-2-8"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="26.7185px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            writingMode="lr-tb"
            textAnchor="start"
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={1.82171}
          >
            <tspan
              style={{
                ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontFeatureSettings: 'normal',
                textAlign: 'start',
              }}
              x={393.69962}
              y={267.75983}
              id="tspan6490-1-1-1"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="26.7185px"
              fontFamily="Franklin Gothic Medium"
              writingMode="lr-tb"
              textAnchor="start"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={1.82171}
            >
              <tspan
                dx={0}
                dy={0}
                style={{
                  ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
                  fontVariantLigatures: 'normal',
                  fontVariantCaps: 'normal',
                  fontVariantNumeric: 'normal',
                  fontFeatureSettings: 'normal',
                  textAlign: 'start',
                }}
                id="tspan6486-7-6-5"
                fontStyle="normal"
                fontVariant="normal"
                fontWeight={400}
                fontStretch="normal"
                fontSize="26.7185px"
                fontFamily="Franklin Gothic Medium"
                writingMode="lr-tb"
                textAnchor="start"
                fill="#fff"
                fillOpacity={1}
                strokeWidth={1.82171}
              >
                {'\u2193'}
              </tspan>
              <tspan
                dx={0}
                dy={0}
                style={{
                  ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
                  fontVariantLigatures: 'normal',
                  fontVariantCaps: 'normal',
                  fontVariantNumeric: 'normal',
                  fontFeatureSettings: 'normal',
                  textAlign: 'start',
                }}
                id="tspan6488-7-3-5"
                fontStyle="normal"
                fontVariant="normal"
                fontWeight={400}
                fontStretch="normal"
                fontSize="26.7185px"
                fontFamily="Franklin Gothic Medium"
                writingMode="lr-tb"
                textAnchor="start"
                fill="#fff"
                fillOpacity={1}
                strokeWidth={1.82171}
              />
            </tspan>
          </text>
          <path
            id="rect2171-5"
            display="inline"
            opacity={0.215}
            fill="url(#radialGradient2179-9)"
            fillOpacity={1}
            stroke="none"
            strokeWidth={2.50149}
            strokeLinecap="square"
            strokeLinejoin="bevel"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="markers stroke fill"
            d="M37.171253 71.264648H39.9444459V295.507658H37.171253z"
          />
          <text
            transform="scale(.91431 1.09372)"
            id="text6507-5-6-4"
            y={79.590248}
            x={44.967297}
            style={{
              lineHeight: 1.25,
              ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontFeatureSettings: 'normal',
              textAlign: 'start',
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="13.1081px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            writingMode="lr-tb"
            textAnchor="start"
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.893723}
          >
            <tspan
              style={{
                ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontFeatureSettings: 'normal',
                textAlign: 'start',
              }}
              y={79.590248}
              x={44.967297}
              id="tspan6505-1-9-8"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="13.1081px"
              fontFamily="Franklin Gothic Medium"
              writingMode="lr-tb"
              textAnchor="start"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.893723}
            >
              {'ALRM'}
            </tspan>
          </text>
          <text
            transform="scale(.80251 1.24609)"
            id="text6511-8-4-91-8"
            y={143.76218}
            x={55.978413}
            style={{
              lineHeight: 1.25,
              ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontFeatureSettings: 'normal',
              textAlign: 'start',
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="14.9342px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            writingMode="lr-tb"
            textAnchor="start"
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={1.01823}
          >
            <tspan
              style={{
                ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontFeatureSettings: 'normal',
                textAlign: 'start',
              }}
              y={143.76218}
              x={55.978413}
              id="tspan6509-1-7-4-6"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="14.9342px"
              fontFamily="Franklin Gothic Medium"
              writingMode="lr-tb"
              textAnchor="start"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={1.01823}
            >
              {'ESC'}
            </tspan>
          </text>
          <text
            transform="scale(.82584 1.2109)"
            id="text6515-9-9-3-1"
            y={219.98798}
            x={54.146912}
            style={{
              lineHeight: 1.25,
              ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontFeatureSettings: 'normal',
              textAlign: 'start',
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="14.5123px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            writingMode="lr-tb"
            textAnchor="start"
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.989475}
          >
            <tspan
              style={{
                ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontFeatureSettings: 'normal',
                textAlign: 'start',
              }}
              y={219.98798}
              x={54.146912}
              id="tspan6513-8-1-2-6"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="14.5123px"
              fontFamily="Franklin Gothic Medium"
              writingMode="lr-tb"
              textAnchor="start"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.989475}
            >
              {'PRG'}
            </tspan>
          </text>
          <path
            id="rect2711-0"
            display="inline"
            opacity={0.946}
            fill="url(#radialGradient2727-1)"
            fillOpacity={1}
            stroke="none"
            strokeWidth={2.50149}
            strokeLinecap="square"
            strokeLinejoin="bevel"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="markers stroke fill"
            d="M17.678272 101.91949H45.671543V103.4786608H17.678272z"
          />
          <ellipse
            ry={8.1568441}
            rx={8.4110956}
            cy={44.520924}
            cx={233.87245}
            id="st"
            display="inline"
            fill={st2}
            fillOpacity={1}
            fillRule="evenodd"
            stroke="none"
            strokeWidth={0.634991}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="markers stroke fill"
          />
          <circle
            transform="matrix(.4119 0 0 .3903 104.63 4.28)"
            r={11.973675}
            cy={94.162521}
            cx={312.73749}
            id="path4259-6-8"
            display="inline"
            opacity={0.65}
            fill="#fff"
            fillOpacity={0.996078}
            fillRule="evenodd"
            stroke="none"
            strokeWidth={7.34263}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="markers stroke fill"
            filter="url(#filter4280-2)"
          />
          <text
            transform="scale(.70565 1.41714)"
            id="tsum"
            y={97.643105}
            x={252.04587}
            style={{
              lineHeight: 1.25,
              ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
              textAlign: 'start',
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="15.875px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            writingMode="lr-tb"
            textAnchor="start"
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={1.01823}
          >
            <tspan
              id="tspan1256-80-45-7-5"
              style={{
                ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
                textAlign: 'start',
              }}
              y={97.643105}
              x={252.04587}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="15.875px"
              fontFamily="Franklin Gothic Medium"
              writingMode="lr-tb"
              textAnchor="start"
              fill="#fff"
              strokeWidth={1.01823}
            >
              {tsum + '°'}
            </tspan>
          </text>
          <text
            transform="scale(.70565 1.41714)"
            id="tret"
            y={121.84331}
            x={251.82883}
            style={{
              lineHeight: 1.25,
              ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
              textAlign: 'start',
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="15.875px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            writingMode="lr-tb"
            textAnchor="start"
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={1.01823}
          >
            <tspan
              id="tspan1256-80-3-2-1-3"
              style={{
                ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
                textAlign: 'start',
              }}
              y={121.84331}
              x={251.82883}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="15.875px"
              fontFamily="Franklin Gothic Medium"
              writingMode="lr-tb"
              textAnchor="start"
              fill="#fff"
              strokeWidth={1.01823}
            >
              {tret + '°'}
            </tspan>
          </text>
          <text
            transform="scale(.70565 1.41714)"
            id="troom"
            y={147.49176}
            x={251.82883}
            style={{
              lineHeight: 1.25,
              ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
              textAlign: 'start',
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="15.875px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            writingMode="lr-tb"
            textAnchor="start"
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={1.01823}
          >
            <tspan
              id="tspan1256-80-3-8-47-8-2"
              style={{
                ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
                textAlign: 'start',
              }}
              y={147.49176}
              x={251.82883}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="15.875px"
              fontFamily="Franklin Gothic Medium"
              writingMode="lr-tb"
              textAnchor="start"
              fill="#fff"
              strokeWidth={1.01823}
            >
              {troom + '°'}
            </tspan>
          </text>
          <text
            transform="scale(.70565 1.41714)"
            id="hum_rel"
            y={171.20616}
            x={251.9296}
            style={{
              lineHeight: 1.25,
              ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
              textAlign: 'start',
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="15.875px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            writingMode="lr-tb"
            textAnchor="start"
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={1.01823}
          >
            <tspan
              id="tspan1256-80-3-8-4-8-3-0"
              style={{
                ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
                textAlign: 'start',
              }}
              y={171.20616}
              x={251.9296}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="15.875px"
              fontFamily="Franklin Gothic Medium"
              writingMode="lr-tb"
              textAnchor="start"
              fill="#fff"
              strokeWidth={1.01823}
            >
              {rel_hum + '%'}
            </tspan>
          </text>
          <text
            transform="scale(.70565 1.41714)"
            id="st_valv"
            y={197.0181}
            x={251.89084}
            style={{
              lineHeight: 1.25,
              ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
              textAlign: 'start',
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="15.875px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            writingMode="lr-tb"
            textAnchor="start"
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={1.01823}
          >
            <tspan
              id="tspan1256-80-3-8-8-46-1-1"
              style={{
                ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
                textAlign: 'start',
              }}
              y={197.0181}
              x={251.89084}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="15.875px"
              fontFamily="Franklin Gothic Medium"
              writingMode="lr-tb"
              textAnchor="start"
              fill="#fff"
              strokeWidth={1.01823}
            >
              {st_valv}
            </tspan>
          </text>
          <text
            transform="scale(.70565 1.41714)"
            id="aper_valsum"
            y={97.643105}
            x={462.82114}
            style={{
              lineHeight: 1.25,
              ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
              textAlign: 'start',
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="15.875px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            writingMode="lr-tb"
            textAnchor="start"
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={1.01823}
          >
            <tspan
              id="tspan1256-80-8-2-5-2"
              style={{
                ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
                textAlign: 'start',
              }}
              y={97.643105}
              x={462.82114}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="15.875px"
              fontFamily="Franklin Gothic Medium"
              writingMode="lr-tb"
              textAnchor="start"
              fill="#fff"
              strokeWidth={1.01823}
            >
              {cold_valv + '%'}
            </tspan>
          </text>
          <text
            transform="scale(.70565 1.41714)"
            id="aper_valret"
            y={121.78905}
            x={462.82114}
            style={{
              lineHeight: 1.25,
              ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
              textAlign: 'start',
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="15.875px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            writingMode="lr-tb"
            textAnchor="start"
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={1.01823}
          >
            <tspan
              id="tspan1256-80-3-86-0-5-7"
              style={{
                ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
                textAlign: 'start',
              }}
              y={121.78905}
              x={462.82114}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="15.875px"
              fontFamily="Franklin Gothic Medium"
              writingMode="lr-tb"
              textAnchor="start"
              fill="#fff"
              strokeWidth={1.01823}
            >
              {cold_valv + '%'}
            </tspan>
          </text>
          <text
            transform="scale(.70565 1.41714)"
            id="horfunc"
            y={147.49176}
            x={462.40256}
            style={{
              lineHeight: 1.25,
              ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
              textAlign: 'start',
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="15.875px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            writingMode="lr-tb"
            textAnchor="start"
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={1.01823}
          >
            <tspan
              id="tspan1256-80-3-8-7-7-7-7"
              style={{
                ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
                textAlign: 'start',
              }}
              y={147.49176}
              x={462.40256}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="15.875px"
              fontFamily="Franklin Gothic Medium"
              writingMode="lr-tb"
              textAnchor="start"
              fill="#fff"
              strokeWidth={1.01823}
            >
              {run_hours + 'H'}
            </tspan>
          </text>
          <text
            transform="scale(.70565 1.41714)"
            id="stfan"
            y={171.20616}
            x={462.88315}
            style={{
              lineHeight: 1.25,
              ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
              textAlign: 'start',
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="15.875px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            writingMode="lr-tb"
            textAnchor="start"
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={1.01823}
          >
            <tspan
              id="tspan1256-80-3-8-4-51-4-8-3"
              style={{
                ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
                textAlign: 'start',
              }}
              y={171.20616}
              x={462.88315}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="15.875px"
              fontFamily="Franklin Gothic Medium"
              writingMode="lr-tb"
              textAnchor="start"
              fill="#fff"
              strokeWidth={1.01823}
            >
              {st_fan}
            </tspan>
          </text>
          <text
            transform="scale(.70565 1.41714)"
            id="per_fan"
            y={198.77097}
            x={462.86765}
            style={{
              lineHeight: 1.25,
              ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
              textAlign: 'start',
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="15.875px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            writingMode="lr-tb"
            textAnchor="start"
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={1.01823}
          >
            <tspan
              id="tspan1256-80-3-8-8-38-8-8-1"
              style={{
                ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
                textAlign: 'start',
              }}
              y={198.77097}
              x={462.86765}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="15.875px"
              fontFamily="Franklin Gothic Medium"
              writingMode="lr-tb"
              textAnchor="start"
              fill="#fff"
              strokeWidth={1.01823}
            >
              {cold_valv + '%'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
              ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              textAlign: 'start',
              fontVariantEastAsian: 'normal',
            }}
            x={135.57201}
            y={97.639488}
            id="lbl2-7"
            transform="scale(.70565 1.41714)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="16.22777792px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            writingMode="lr-tb"
            textAnchor="start"
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={1.01824}
          >
            <tspan
              x={135.57201}
              y={97.639488}
              style={{
                ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                textAlign: 'start',
                fontVariantEastAsian: 'normal',
              }}
              id="tspan1256-93-3-3"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="16.22777792px"
              fontFamily="Franklin Gothic Medium"
              writingMode="lr-tb"
              textAnchor="start"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={1.01824}
            >
              {'TEMP SUM'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
              ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
              textAlign: 'start',
            }}
            x={135.57201}
            y={121.8614}
            id="lbl3-9"
            transform="scale(.70565 1.41714)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="16.2278px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            writingMode="lr-tb"
            textAnchor="start"
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={1.01824}
          >
            <tspan
              x={135.57201}
              y={121.8614}
              style={{
                ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
                textAlign: 'start',
              }}
              id="tspan1256-8-6-2-6"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="16.2278px"
              fontFamily="Franklin Gothic Medium"
              writingMode="lr-tb"
              textAnchor="start"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={1.01824}
            >
              {'TEMP RET'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
              ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
              textAlign: 'start',
            }}
            x={134.38345}
            y={171.20255}
            id="lbl5-9"
            transform="scale(.70565 1.41714)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="16.2278px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            writingMode="lr-tb"
            textAnchor="start"
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={1.01824}
          >
            <tspan
              x={134.38345}
              y={171.20255}
              style={{
                ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
                textAlign: 'start',
              }}
              id="tspan1256-0-27-5-2"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="16.2278px"
              fontFamily="Franklin Gothic Medium"
              writingMode="lr-tb"
              textAnchor="start"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={1.01824}
            >
              {'HUM RELT'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
              ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
              textAlign: 'start',
            }}
            x={134.38345}
            y={197.01448}
            id="lbl6-9"
            transform="scale(.70565 1.41714)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="16.2278px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            writingMode="lr-tb"
            textAnchor="start"
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={1.01824}
          >
            <tspan
              x={134.38345}
              y={197.01448}
              style={{
                ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
                textAlign: 'start',
              }}
              id="tspan1256-5-57-7-8"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="16.2278px"
              fontFamily="Franklin Gothic Medium"
              writingMode="lr-tb"
              textAnchor="start"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={1.01824}
            >
              {'ESTADO VALV'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
              ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
              textAlign: 'start',
            }}
            x={135.57201}
            y={147.48814}
            id="lbl4-1"
            transform="scale(.70565 1.41714)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="16.2278px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            writingMode="lr-tb"
            textAnchor="start"
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={1.01824}
          >
            <tspan
              x={135.57201}
              y={147.48814}
              style={{
                ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
                textAlign: 'start',
              }}
              id="tspan1256-58-3-1-8"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="16.2278px"
              fontFamily="Franklin Gothic Medium"
              writingMode="lr-tb"
              textAnchor="start"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={1.01824}
            >
              {'TEMP ROOM'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
              ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
              textAlign: 'start',
            }}
            x={331.26978}
            y={97.639488}
            id="lbl7-1"
            transform="scale(.70565 1.41714)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="16.2278px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            writingMode="lr-tb"
            textAnchor="start"
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={1.01824}
          >
            <tspan
              x={331.26978}
              y={97.639488}
              style={{
                ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
                textAlign: 'start',
              }}
              id="tspan1256-09-5-9-1"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="16.2278px"
              fontFamily="Franklin Gothic Medium"
              writingMode="lr-tb"
              textAnchor="start"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={1.01824}
            >
              {'APER VALV SUM'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
              ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
              textAlign: 'start',
            }}
            x={331.26978}
            y={121.8614}
            id="lbl8-9"
            transform="scale(.70565 1.41714)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="16.2278px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            writingMode="lr-tb"
            textAnchor="start"
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={1.01824}
          >
            <tspan
              x={331.26978}
              y={121.8614}
              style={{
                ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
                textAlign: 'start',
              }}
              id="tspan1256-8-8-3-8-2"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="16.2278px"
              fontFamily="Franklin Gothic Medium"
              writingMode="lr-tb"
              textAnchor="start"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={1.01824}
            >
              {'APER VALV RET'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
              ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
              textAlign: 'start',
            }}
            x={330.13669}
            y={147.48814}
            id="lbl9-8"
            transform="scale(.70565 1.41714)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="16.2278px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            writingMode="lr-tb"
            textAnchor="start"
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={1.01824}
          >
            <tspan
              x={330.13669}
              y={147.48814}
              style={{
                ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
                textAlign: 'start',
              }}
              id="tspan1256-0-29-0-6-4"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="16.2278px"
              fontFamily="Franklin Gothic Medium"
              writingMode="lr-tb"
              textAnchor="start"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={1.01824}
            >
              {'HORAS FUNC'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
              ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
              textAlign: 'start',
            }}
            x={330.81021}
            y={198.76735}
            id="lbl11-1"
            transform="scale(.70565 1.41714)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="16.2278px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            writingMode="lr-tb"
            textAnchor="start"
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={1.01824}
          >
            <tspan
              x={330.81021}
              y={198.76735}
              style={{
                ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
                textAlign: 'start',
              }}
              id="tspan1256-5-4-9-6-1"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="16.2278px"
              fontFamily="Franklin Gothic Medium"
              writingMode="lr-tb"
              textAnchor="start"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={1.01824}
            >
              {'% FUNC FAN'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
              ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
              textAlign: 'start',
            }}
            x={330.13669}
            y={171.20255}
            id="lbl10-4"
            transform="scale(.70565 1.41714)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="16.2278px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            writingMode="lr-tb"
            textAnchor="start"
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={1.01824}
          >
            <tspan
              x={330.13669}
              y={171.20255}
              style={{
                ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
                textAlign: 'start',
              }}
              id="tspan1256-58-79-6-6-9"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="16.2278px"
              fontFamily="Franklin Gothic Medium"
              writingMode="lr-tb"
              textAnchor="start"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={1.01824}
            >
              {'ESTADO FAN'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={242.67749}
            y={93.278076}
            id="lbl12-6"
            transform="scale(.70565 1.41714)"
            fontStyle="normal"
            fontWeight={400}
            fontSize="10.3232px"
            fontFamily="sans-serif"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={1.01824}
          >
            <tspan
              id="tspan1306-5-5-6"
              x={242.67749}
              y={93.278076}
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={1.01824}
            >
              {':'}
            </tspan>
            <tspan
              x={242.67749}
              y={119.08607}
              id="tspan1312-0-5-2"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={1.01824}
            >
              {':'}
            </tspan>
            <tspan
              x={242.67749}
              y={144.89407}
              id="tspan1324-46-9-7"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={1.01824}
            >
              {':'}
            </tspan>
            <tspan
              x={242.67749}
              y={170.70207}
              id="tspan1326-44-8-3"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={1.01824}
            >
              {':'}
            </tspan>
            <tspan
              x={242.67749}
              y={196.51007}
              id="tspan1328-9-8-2"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={1.01824}
            >
              {':'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={454.8186}
            y={95.030983}
            id="lbl13-2"
            transform="scale(.70565 1.41714)"
            fontStyle="normal"
            fontWeight={400}
            fontSize="10.3232px"
            fontFamily="sans-serif"
            letterSpacing={0}
            wordSpacing={0}
            display="inline"
            fill="#00aad4"
            fillOpacity={1}
            stroke="none"
            strokeWidth={1.01824}
          >
            <tspan
              id="tspan1306-50-8-2-9"
              x={454.8186}
              y={95.030983}
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={1.01824}
            >
              {':'}
            </tspan>
            <tspan
              x={454.8186}
              y={120.83898}
              id="tspan1312-3-2-0-0"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={1.01824}
            >
              {':'}
            </tspan>
            <tspan
              x={454.8186}
              y={146.64699}
              id="tspan1324-4-3-4-1"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={1.01824}
            >
              {':'}
            </tspan>
            <tspan
              x={454.8186}
              y={172.45499}
              id="tspan1326-17-1-4-8"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={1.01824}
            >
              {':'}
            </tspan>
            <tspan
              x={454.8186}
              y={198.26299}
              id="tspan1328-58-4-6-1"
              fill="#00aad4"
              fillOpacity={1}
              strokeWidth={1.01824}
            >
              {':'}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
              ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              textAlign: 'start',
              fontVariantEastAsian: 'normal',
            }}
            x={256.94485}
            y={71.231491}
            id="lbl1-2"
            transform="scale(.70565 1.41714)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="16.5806px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            writingMode="lr-tb"
            textAnchor="start"
            display="inline"
            fill="#d4aa00"
            fillOpacity={1}
            stroke="none"
            strokeWidth={1.01824}
          >
            <tspan
              x={256.94485}
              y={71.231491}
              style={{
                ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                textAlign: 'start',
                fontVariantEastAsian: 'normal',
              }}
              id="tspan1256-09-5-9-5-77"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="16.5805557px"
              fontFamily="Franklin Gothic Medium"
              writingMode="lr-tb"
              textAnchor="start"
              fill="#d4aa00"
              fillOpacity={1}
              strokeWidth={1.01824}
            >
              {'DATOS GENERALES'}
            </tspan>
          </text>
          <path
            id="path3291-2"
            d="M25.427 305.588l-14.255-18.55V47.449l15.443-18.035h386.07l14.652 17.004v240.107l-14.255 18.033z"
            display="inline"
            fill="none"
            stroke="#3fdce4"
            strokeWidth="1.01824px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            id="path3295-3"
            d="M85.573 13.671l5.53-7.196h8.47l1.315 2.965h260.619l1.663-2.166h8.836l4.558 5.931h-4.418l-.596-1.343H91.033l-.895 2.017z"
            display="inline"
            fill="#168198"
            fillOpacity={1}
            stroke="#3fdce4"
            strokeWidth="1.01824px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            d="M417.925 330.2l-1.749 4.492h6.416l.737 1.975c6.315-.497 10.835-3.453 15.047-6.83l.184-3.462 13.082-19.4 1.842.272 2.088-8.106-1.29-.73V283.11h-2.886v18.217c-16.99 24.856-18.6 30.447-33.47 28.873z"
            id="path1059-2-5-75"
            display="inline"
            fill="none"
            fillOpacity={1}
            stroke="#3fdce4"
            strokeWidth={1.01983}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M86.351 329.344l5.53 7.196h8.47l1.315-2.965h260.618l1.665 2.166h8.835l4.558-5.931h-4.418l-.596 1.343H91.811l-.895-2.017z"
            id="path3295-6-5"
            display="inline"
            fill="#168198"
            fillOpacity={1}
            stroke="#3fdce4"
            strokeWidth="1.01824px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <g
            id="g1264-5"
            transform="matrix(4.23297 0 0 5.50808 -82.843 -518.898)"
            display="inline"
            fill="#168198"
            fillOpacity={1}
            stroke="#3fdce4"
            strokeWidth=".210875px"
            strokeOpacity={1}
            strokeLinecap="butt"
            strokeLinejoin="miter"
          >
            <path id="path1197-44" d="M30.577 96.918l.472-.85h.551l-.481.853z" />
            <path d="M31.555 96.918l.472-.85h.551l-.481.853z" id="path1197-0-2" display="inline" />
            <path d="M32.555 96.918l.472-.85h.551l-.482.853z" id="path1197-7-7" display="inline" />
            <path d="M33.52 96.918l.471-.85h.552l-.482.853z" id="path1197-03-5" display="inline" />
            <path d="M34.52 96.918l.471-.85h.552l-.482.853z" id="path1197-4-0" display="inline" />
            <path d="M35.472 96.918l.472-.85h.552l-.482.853z" id="path1197-3-0" display="inline" />
            <path d="M36.472 96.918l.472-.85h.551l-.481.853z" id="path1197-1-7" display="inline" />
            <path d="M37.536 96.918l.472-.85h.551l-.481.853z" id="path1197-14-0" display="inline" />
          </g>
          <g
            id="g1264-1-1"
            transform="matrix(-4.23297 0 0 5.50808 545.072 -519.396)"
            display="inline"
            fill="#168198"
            fillOpacity={1}
            stroke="#3fdce4"
            strokeWidth=".210875px"
            strokeOpacity={1}
            strokeLinecap="butt"
            strokeLinejoin="miter"
          >
            <path d="M30.577 96.918l.472-.85h.551l-.481.853z" id="path1197-39-4" />
            <path id="path1197-0-9-7" d="M31.555 96.918l.472-.85h.551l-.481.853z" display="inline" />
            <path id="path1197-7-9-5" d="M32.555 96.918l.472-.85h.551l-.482.853z" display="inline" />
            <path id="path1197-03-1-0" d="M33.52 96.918l.471-.85h.552l-.482.853z" display="inline" />
            <path id="path1197-4-1-8" d="M34.52 96.918l.471-.85h.552l-.482.853z" display="inline" />
            <path id="path1197-3-6-2" d="M35.472 96.918l.472-.85h.552l-.482.853z" display="inline" />
            <path id="path1197-1-3-4" d="M36.472 96.918l.472-.85h.551l-.481.853z" display="inline" />
            <path id="path1197-14-4-5" d="M37.536 96.918l.472-.85h.551l-.481.853z" display="inline" />
          </g>
          <g
            transform="matrix(4.23297 0 0 5.50808 251.103 -199.189)"
            id="g1264-1-6-1"
            display="inline"
            fill="#168198"
            fillOpacity={1}
            stroke="#3fdce4"
            strokeWidth=".210875px"
            strokeOpacity={1}
            strokeLinecap="butt"
            strokeLinejoin="miter"
          >
            <path id="path1197-39-5-5" d="M30.577 96.918l.472-.85h.551l-.481.853z" />
            <path d="M31.555 96.918l.472-.85h.551l-.481.853z" id="path1197-0-9-5-0" display="inline" />
            <path d="M32.555 96.918l.472-.85h.551l-.482.853z" id="path1197-7-9-6-4" display="inline" />
            <path d="M33.52 96.918l.471-.85h.552l-.482.853z" id="path1197-03-1-2-4" display="inline" />
            <path d="M34.52 96.918l.471-.85h.552l-.482.853z" id="path1197-4-1-0-3" display="inline" />
            <path d="M35.472 96.918l.472-.85h.552l-.482.853z" id="path1197-3-6-4-3" display="inline" />
            <path d="M36.472 96.918l.472-.85h.551l-.481.853z" id="path1197-1-3-9-8" display="inline" />
            <path d="M37.536 96.918l.472-.85h.551l-.481.853z" id="path1197-14-4-9-7" display="inline" />
          </g>
          <g
            transform="matrix(-4.23297 0 0 5.50808 210.346 -199.58)"
            id="g1264-1-5-1"
            display="inline"
            fill="#168198"
            fillOpacity={1}
            stroke="#3fdce4"
            strokeWidth=".210875px"
            strokeOpacity={1}
            strokeLinecap="butt"
            strokeLinejoin="miter"
          >
            <path id="path1197-39-3-7" d="M30.577 96.918l.472-.85h.551l-.481.853z" />
            <path d="M31.555 96.918l.472-.85h.551l-.481.853z" id="path1197-0-9-0-0" display="inline" />
            <path d="M32.555 96.918l.472-.85h.551l-.482.853z" id="path1197-7-9-9-1" display="inline" />
            <path d="M33.52 96.918l.471-.85h.552l-.482.853z" id="path1197-03-1-3-9" display="inline" />
            <path d="M34.52 96.918l.471-.85h.552l-.482.853z" id="path1197-4-1-9-5" display="inline" />
            <path d="M35.472 96.918l.472-.85h.552l-.482.853z" id="path1197-3-6-6-3" display="inline" />
            <path d="M36.472 96.918l.472-.85h.551l-.481.853z" id="path1197-1-3-3-3" display="inline" />
            <path d="M37.536 96.918l.472-.85h.551l-.481.853z" id="path1197-14-4-4-1" display="inline" />
          </g>
          <g
            id="g1412-1"
            transform="matrix(4.23297 0 0 5.50808 -80.83 -517.701)"
            display="inline"
            fill="#168198"
            fillOpacity={1}
            stroke="#3fdce4"
            strokeWidth={0.120347}
            strokeOpacity={1}
            fillRule="evenodd"
            strokeLinecap="square"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            paintOrder="markers stroke fill"
          >
            <path id="rect1386-0" opacity={1} d="M20.369278 105.98573H21.211177040000003V106.43006558H20.369278z" />
            <path
              id="rect1386-5-40"
              display="inline"
              opacity={1}
              d="M20.369278 106.77502H21.211177040000003V107.21935561H20.369278z"
            />
            <path
              id="rect1386-1-6"
              display="inline"
              opacity={1}
              d="M20.369278 107.56429H21.211177040000003V108.00862561H20.369278z"
            />
          </g>
          <g
            id="g1412-2-1"
            transform="matrix(4.23297 0 0 5.50808 -80.631 -315.755)"
            display="inline"
            fill="#168198"
            fillOpacity={1}
            stroke="#3fdce4"
            strokeWidth={0.120347}
            strokeOpacity={1}
            fillRule="evenodd"
            strokeLinecap="square"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            paintOrder="markers stroke fill"
          >
            <path id="rect1386-3-4" opacity={1} d="M20.369278 105.98573H21.211177040000003V106.43006558H20.369278z" />
            <path
              id="rect1386-5-4-5"
              display="inline"
              opacity={1}
              d="M20.369278 106.77502H21.211177040000003V107.21935561H20.369278z"
            />
            <path
              id="rect1386-1-2-8"
              display="inline"
              opacity={1}
              d="M20.369278 107.56429H21.211177040000003V108.00862561H20.369278z"
            />
          </g>
          <g
            id="g1412-5-1"
            transform="matrix(4.23297 0 0 5.50808 364.44 -315.24)"
            display="inline"
            fill="#168198"
            fillOpacity={1}
            stroke="#3fdce4"
            strokeWidth={0.120347}
            strokeOpacity={1}
            fillRule="evenodd"
            strokeLinecap="square"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            paintOrder="markers stroke fill"
          >
            <path id="rect1386-34-0" opacity={1} d="M20.369278 105.98573H21.211177040000003V106.43006558H20.369278z" />
            <path
              id="rect1386-5-49-8"
              display="inline"
              opacity={1}
              d="M20.369278 106.77502H21.211177040000003V107.21935561H20.369278z"
            />
            <path
              id="rect1386-1-5-0"
              display="inline"
              opacity={1}
              d="M20.369278 107.56429H21.211177040000003V108.00862561H20.369278z"
            />
          </g>
          <g
            id="g1412-0-6"
            transform="matrix(4.23297 0 0 5.50808 365.899 -519.666)"
            display="inline"
            fill="#168198"
            fillOpacity={1}
            stroke="#3fdce4"
            strokeWidth={0.120347}
            strokeOpacity={1}
            fillRule="evenodd"
            strokeLinecap="square"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            paintOrder="markers stroke fill"
          >
            <path id="rect1386-7-4" opacity={1} d="M20.369278 105.98573H21.211177040000003V106.43006558H20.369278z" />
            <path
              id="rect1386-5-7-8"
              display="inline"
              opacity={1}
              d="M20.369278 106.77502H21.211177040000003V107.21935561H20.369278z"
            />
            <path
              id="rect1386-1-9-8"
              display="inline"
              opacity={1}
              d="M20.369278 107.56429H21.211177040000003V108.00862561H20.369278z"
            />
          </g>
          <g
            id="g1640-6"
            transform="matrix(4.23297 0 0 5.50808 -559.942 -340.89)"
            display="inline"
            strokeWidth={0.0956409}
            strokeMiterlimit={4}
            strokeDasharray="none"
            stroke="#3fdbe4"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          >
            <path id="path1391-89" d="M141.784 98.805V87.131h7.64v11.774z" fill="none" />
            <path
              id="path1393-3"
              d="M140.86 99.56h1.497l.72.96h4.12l.814-.971h1.625l.631-.65v-2.56l-.462-.304v-6.044l.487-.496v-2.53l-.48-.52h-8.988l-.71.735v1.836l.917.884v5.887l-.934.819v1.802z"
              fill="none"
            />
            <path
              id="path1395-8"
              d="M142.725 100.075h-1.982l-.853-1.31V86.862l.812-.906h1.216l.52-.684h4.836l.713.73h.743l.42.45"
              fill="none"
            />
            <path id="path1397-9" d="M139.584 98.078v-2.373l-.64-.484v-5.077l.615-.45V87.7" fill="none" />
            <path id="path1399-73" d="M139.542 95.405l-.45-.32v-4.873l.45-.263z" fill="#377e96" fillOpacity={1} />
            <path id="path1401-3" d="M139.896 96.304l.817-.665v-5.637l-.829-.701z" fill="#377e96" fillOpacity={1} />
            <path id="path1403-0" d="M141.059 98.707v-2.391l-.52.362v1.614z" fill="#377e96" fillOpacity={1} />
            <path id="path1405-3" d="M146.951 100.186l.505-.67h-4.664l.505.654z" fill="none" />
            <path id="path1407-0" d="M142.491 86.154h4.935l-.55-.608h-4.01z" fill="none" fillOpacity={1} />
            <path id="path1409-7" d="M150.639 88.503v1.15l-.513.438v5.746l.496.347v.786" fill="none" />
            <path id="path1411-5" d="M149.8 98.952l.198-.187v-2.157l-.193-.076z" fill="#377e96" fillOpacity={1} />
            <path
              d="M141.115 89.382V86.99l-.52.362v1.614z"
              id="path1403-1-9"
              display="inline"
              fill="#377e96"
              fillOpacity={1}
            />
            <path
              d="M149.852 89.385l.2-.187v-2.157l-.194-.076z"
              id="path1411-4-3"
              display="inline"
              fill="#377e96"
              fillOpacity={1}
            />
          </g>
          <g
            id="g1662-3"
            transform="matrix(4.23297 0 0 5.50808 -140.411 -64.2)"
            display="inline"
            strokeWidth={0.0956409}
            strokeMiterlimit={4}
            strokeDasharray="none"
            stroke="#3fdbe4"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          >
            <g transform="matrix(1 0 0 -1 -98.82 151.633)" id="g1496-8-7" display="inline">
              <path id="path1391-8-2" d="M141.784 98.805V87.131h7.64v11.774z" fill="none" />
              <path
                id="path1393-9-6"
                d="M140.86 99.56h1.497l.72.96h4.12l.814-.971h1.625l.631-.65v-2.56l-.462-.304v-6.044l.487-.496v-2.53l-.48-.52h-8.988l-.71.735v1.836l.917.884v5.887l-.934.819v1.802z"
                fill="none"
              />
              <path
                id="path1395-4-1"
                d="M142.725 100.075h-1.982l-.853-1.31V86.862l.812-.906h1.216l.52-.684h4.836l.713.73h.743l.42.45"
                fill="none"
              />
              <path id="path1397-8-0" d="M139.584 98.078v-2.373l-.64-.484v-5.077l.615-.45V87.7" fill="none" />
              <path id="path1399-3-1" d="M139.542 95.405l-.45-.32v-4.873l.45-.263z" fill="#377e96" fillOpacity={1} />
              <path id="path1401-4-4" d="M139.896 96.304l.817-.665v-5.637l-.829-.701z" fill="#377e96" fillOpacity={1} />
              <path id="path1403-8-0" d="M141.059 98.707v-2.391l-.52.362v1.614z" fill="#377e96" fillOpacity={1} />
              <path id="path1405-6-5" d="M146.951 100.186l.505-.67h-4.664l.505.654z" fill="none" />
              <path id="path1407-6-5" d="M142.491 86.154h4.935l-.55-.608h-4.01z" fill="#377e96" fillOpacity={1} />
              <path id="path1409-5-0" d="M150.639 88.503v1.15l-.513.438v5.746l.496.347v.786" fill="none" />
              <path id="path1411-8-9" d="M149.8 98.952l.198-.187v-2.157l-.193-.076z" fill="#377e96" fillOpacity={1} />
              <path
                d="M141.115 89.382V86.99l-.52.362v1.614z"
                id="path1403-1-4-4"
                display="inline"
                fill="#377e96"
                fillOpacity={1}
              />
            </g>
            <path
              d="M51.048 64.564l.199-.187V62.22l-.193-.076z"
              id="path1411-3-5"
              display="inline"
              fill="#377e96"
              fillOpacity={1}
            />
          </g>
          <path
            id="path1391-9-2"
            d="M40.499 116.31V52.004h32.34v64.853z"
            display="inline"
            fill="none"
            stroke="#3fdbe4"
            strokeWidth={0.461814}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path1393-4-9"
            d="M36.59 120.469h6.336l3.044 5.281h17.447l3.44-5.345h6.88l2.673-3.575v-14.105l-1.955-1.675V67.762l2.058-2.733V51.093l-2.03-2.869H36.44l-3.01 4.054v10.11l3.885 4.873v32.426l-3.955 4.509v9.928z"
            display="inline"
            fill="none"
            stroke="#3fdbe4"
            strokeWidth={0.461814}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path1395-5-6"
            d="M44.485 123.303h-8.39l-3.613-7.214V50.524l3.44-4.992h5.148l2.202-3.768H63.74l3.02 4.026H69.9l1.782 2.48"
            display="inline"
            fill="none"
            stroke="#3fdbe4"
            strokeWidth={0.461814}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path1397-6-4"
            d="M31.19 112.302v-13.07l-2.713-2.665V68.604l2.607-2.482V55.147"
            display="inline"
            fill="none"
            stroke="#3fdbe4"
            strokeWidth={0.461814}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path1399-7-9"
            d="M31.01 97.578l-1.9-1.757V68.976l1.9-1.45z"
            display="inline"
            fill="#377e96"
            fillOpacity={1}
            stroke="#3fdbe4"
            strokeWidth={0.461814}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path1401-8-1"
            d="M32.51 102.533l3.457-3.666V67.82l-3.51-3.86z"
            display="inline"
            fill="#377e96"
            fillOpacity={1}
            stroke="#3fdbe4"
            strokeWidth={0.461814}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path1403-13-4"
            d="M37.432 115.767v-13.17l-2.203 1.996v8.888z"
            display="inline"
            fill="#377e96"
            fillOpacity={1}
            stroke="#3fdbe4"
            strokeWidth={0.461814}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path1405-5-3"
            d="M62.373 123.916l2.135-3.69h-19.74l2.136 3.599z"
            display="inline"
            fill="none"
            stroke="#3fdbe4"
            strokeWidth={0.461814}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path1407-7-7"
            d="M43.495 46.627h20.888l-2.327-3.35H45.08z"
            display="inline"
            fill="#377e96"
            fillOpacity={1}
            stroke="#3fdbe4"
            strokeWidth={0.461814}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path1409-8-8"
            d="M77.983 59.564v6.33l-2.17 2.414V99.96l2.1 1.913v4.327"
            display="inline"
            fill="none"
            stroke="#3fdbe4"
            strokeWidth={0.461814}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path1411-9-3"
            d="M74.43 117.12l.842-1.03v-11.884l-.817-.418z"
            display="inline"
            fill="#377e96"
            fillOpacity={1}
            stroke="#3fdbe4"
            strokeWidth={0.461814}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            d="M37.667 64.405V51.234l-2.203 1.996v8.888z"
            id="path1403-1-5-0"
            display="inline"
            fill="#377e96"
            fillOpacity={1}
            stroke="#3fdbe4"
            strokeWidth={0.461814}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path1411-45-1"
            d="M74.771 64.945l.842-1.03V52.032l-.817-.419z"
            display="inline"
            fill="#377e96"
            fillOpacity={1}
            stroke="#3fdbe4"
            strokeWidth={0.461814}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <g
            id="g1730-3-9"
            transform="matrix(-4.23297 0 0 5.50808 596.841 -74.942)"
            display="inline"
            strokeWidth={0.0956409}
            strokeMiterlimit={4}
            strokeDasharray="none"
            stroke="#3fdbe4"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          >
            <g id="g1640-3-9" transform="translate(-100.417 -48.301)">
              <path id="path1391-99-8" d="M141.784 98.805V87.131h7.64v11.774z" fill="none" />
              <path
                id="path1393-1-4"
                d="M140.86 99.56h1.497l.72.96h4.12l.814-.971h1.625l.631-.65v-2.56l-.462-.304v-6.044l.487-.496v-2.53l-.48-.52h-8.988l-.71.735v1.836l.917.884v5.887l-.934.819v1.802z"
                fill="none"
              />
              <path
                id="path1395-2-3"
                d="M142.725 100.075h-1.982l-.853-1.31V86.862l.812-.906h1.216l.52-.684h4.836l.713.73h.743l.42.45"
                fill="none"
              />
              <path id="path1397-1-5" d="M139.584 98.078v-2.373l-.64-.484v-5.077l.615-.45V87.7" fill="none" />
              <path id="path1399-6-0" d="M139.542 95.405l-.45-.32v-4.873l.45-.263z" fill="#377e96" fillOpacity={1} />
              <path id="path1401-1-2" d="M139.896 96.304l.817-.665v-5.637l-.829-.701z" fill="#377e96" fillOpacity={1} />
              <path id="path1403-3-8" d="M141.059 98.707v-2.391l-.52.362v1.614z" fill="#377e96" fillOpacity={1} />
              <path id="path1405-51-0" d="M146.951 100.186l.505-.67h-4.664l.505.654z" fill="none" />
              <path id="path1407-77-6" d="M142.491 86.154h4.935l-.55-.608h-4.01z" fill="none" fillOpacity={1} />
              <path id="path1409-4-4" d="M150.639 88.503v1.15l-.513.438v5.746l.496.347v.786" fill="none" />
              <path id="path1411-1-5" d="M149.8 98.952l.198-.187v-2.157l-.193-.076z" fill="#377e96" fillOpacity={1} />
              <path
                d="M141.115 89.382V86.99l-.52.362v1.614z"
                id="path1403-1-3-4"
                display="inline"
                fill="#377e96"
                fillOpacity={1}
              />
              <path
                d="M149.852 89.385l.2-.187v-2.157l-.194-.076z"
                id="path1411-4-5-6"
                display="inline"
                fill="#377e96"
                fillOpacity={1}
              />
            </g>
            <g id="g1662-4-8" transform="translate(-1.307 1.932)">
              <g transform="matrix(1 0 0 -1 -98.82 151.633)" id="g1496-8-0-6" display="inline">
                <path id="path1391-8-3-9" d="M141.784 98.805V87.131h7.64v11.774z" fill="none" />
                <path
                  id="path1393-9-8-1"
                  d="M140.86 99.56h1.497l.72.96h4.12l.814-.971h1.625l.631-.65v-2.56l-.462-.304v-6.044l.487-.496v-2.53l-.48-.52h-8.988l-.71.735v1.836l.917.884v5.887l-.934.819v1.802z"
                  fill="none"
                />
                <path
                  id="path1395-4-0-6"
                  d="M142.725 100.075h-1.982l-.853-1.31V86.862l.812-.906h1.216l.52-.684h4.836l.713.73h.743l.42.45"
                  fill="none"
                />
                <path id="path1397-8-1-2" d="M139.584 98.078v-2.373l-.64-.484v-5.077l.615-.45V87.7" fill="none" />
                <path
                  id="path1399-3-7-2"
                  d="M139.542 95.405l-.45-.32v-4.873l.45-.263z"
                  fill="#377e96"
                  fillOpacity={1}
                />
                <path
                  id="path1401-4-9-5"
                  d="M139.896 96.304l.817-.665v-5.637l-.829-.701z"
                  fill="#377e96"
                  fillOpacity={1}
                />
                <path id="path1403-8-6-4" d="M141.059 98.707v-2.391l-.52.362v1.614z" fill="#377e96" fillOpacity={1} />
                <path id="path1405-6-3-7" d="M146.951 100.186l.505-.67h-4.664l.505.654z" fill="none" />
                <path id="path1407-6-2-4" d="M142.491 86.154h4.935l-.55-.608h-4.01z" fill="#377e96" fillOpacity={1} />
                <path id="path1409-5-1-0" d="M150.639 88.503v1.15l-.513.438v5.746l.496.347v.786" fill="none" />
                <path
                  id="path1411-8-3-4"
                  d="M149.8 98.952l.198-.187v-2.157l-.193-.076z"
                  fill="#377e96"
                  fillOpacity={1}
                />
                <path
                  d="M141.115 89.382V86.99l-.52.362v1.614z"
                  id="path1403-1-4-9-2"
                  display="inline"
                  fill="#377e96"
                  fillOpacity={1}
                />
              </g>
              <path
                d="M51.048 64.564l.199-.187V62.22l-.193-.076z"
                id="path1411-3-1-2"
                display="inline"
                fill="#377e96"
                fillOpacity={1}
              />
            </g>
            <g id="g1684-0-2" transform="translate(-.792 -1.482)">
              <g transform="translate(-99.56 -62.62)" id="g1496-9-6-0" display="inline">
                <path id="path1391-9-7-7" d="M141.784 98.805V87.131h7.64v11.774z" fill="none" />
                <path
                  id="path1393-4-7-4"
                  d="M140.86 99.56h1.497l.72.96h4.12l.814-.971h1.625l.631-.65v-2.56l-.462-.304v-6.044l.487-.496v-2.53l-.48-.52h-8.988l-.71.735v1.836l.917.884v5.887l-.934.819v1.802z"
                  fill="none"
                />
                <path
                  id="path1395-5-7-9"
                  d="M142.725 100.075h-1.982l-.853-1.31V86.862l.812-.906h1.216l.52-.684h4.836l.713.73h.743l.42.45"
                  fill="none"
                />
                <path id="path1397-6-5-4" d="M139.584 98.078v-2.373l-.64-.484v-5.077l.615-.45V87.7" fill="none" />
                <path
                  id="path1399-7-7-7"
                  d="M139.542 95.405l-.45-.32v-4.873l.45-.263z"
                  fill="#377e96"
                  fillOpacity={1}
                />
                <path
                  id="path1401-8-6-0"
                  d="M139.896 96.304l.817-.665v-5.637l-.829-.701z"
                  fill="#377e96"
                  fillOpacity={1}
                />
                <path id="path1403-13-5-0" d="M141.059 98.707v-2.391l-.52.362v1.614z" fill="#377e96" fillOpacity={1} />
                <path id="path1405-5-2-9" d="M146.951 100.186l.505-.67h-4.664l.505.654z" fill="none" />
                <path id="path1407-7-1-2" d="M142.491 86.154h4.935l-.55-.608h-4.01z" fill="#377e96" fillOpacity={1} />
                <path id="path1409-8-1-0" d="M150.639 88.503v1.15l-.513.438v5.746l.496.347v.786" fill="none" />
                <path
                  id="path1411-9-2-7"
                  d="M149.8 98.952l.198-.187v-2.157l-.193-.076z"
                  fill="#377e96"
                  fillOpacity={1}
                />
                <path
                  d="M141.115 89.382V86.99l-.52.362v1.614z"
                  id="path1403-1-5-8-2"
                  display="inline"
                  fill="#377e96"
                  fillOpacity={1}
                />
              </g>
              <path
                d="M50.32 26.861l.2-.187v-2.157l-.194-.076z"
                id="path1411-45-0-6"
                display="inline"
                fill="#377e96"
                fillOpacity={1}
              />
            </g>
          </g>
          <path
            id="path1125-9"
            d="M11.172 47.447L31.366 21.17h397.356l20.393 27.309v249.896l-19.403 26.02H31.762l-20.788-28.338z"
            display="inline"
            fill="none"
            stroke="#3fdbe4"
            strokeWidth="1.01824px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <path
            id="path1059-2-5-3-8"
            d="M419.061 14.092L417.313 9.6h6.416l.737-1.975c6.315.497 10.835 3.453 15.047 6.83l.184 3.462 13.081 19.4 1.843-.272 2.088 8.106-1.29.73v15.301h-2.886V42.965c-16.991-24.856-18.6-30.447-33.471-28.873z"
            display="inline"
            fill="none"
            fillOpacity={1}
            stroke="#3fdce4"
            strokeWidth={1.01983}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path1059-2-5-7-5"
            d="M41.943 16.668l1.749-4.491h-6.416l-.737-1.976c-6.315.498-10.835 3.453-15.047 6.831l-.184 3.461L8.227 39.894l-1.843-.273-2.088 8.106 1.29.729v15.302h2.886V45.541c16.991-24.856 18.6-30.447 33.471-28.873z"
            display="inline"
            fill="none"
            fillOpacity={1}
            stroke="#3fdce4"
            strokeWidth={1.01983}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <path
            id="path1059-2-5-9-4"
            d="M41.745 329.315l1.749 4.492h-6.416l-.737 1.975c-6.315-.498-10.835-3.453-15.047-6.83l-.184-3.462-13.08-19.401-1.843.273-2.088-8.106 1.29-.73v-15.301h2.886v18.217c16.99 24.856 18.6 30.447 33.47 28.873z"
            display="inline"
            fill="none"
            fillOpacity={1}
            stroke="#3fdce4"
            strokeWidth={1.01983}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
              ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              textAlign: 'start',
              fontVariantEastAsian: 'normal',
            }}
            x={147.52821}
            y={42.92942}
            id="nombre"
            transform="scale(.74068 1.35011)"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="15.5222px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            writingMode="lr-tb"
            textAnchor="start"
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={1.06878}
          >
            <tspan
              x={147.52821}
              y={42.92942}
              style={{
                ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                textAlign: 'start',
                fontVariantEastAsian: 'normal',
              }}
              id="tspan1256-09-5-9-5-7-2"
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="15.52222222px"
              fontFamily="Franklin Gothic Medium"
              writingMode="lr-tb"
              textAnchor="start"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={1.06878}
            >
              <a href={url} style={{ fill: 'white' }}>
                {nombre}
              </a>
            </tspan>
          </text>
          <text
            transform="scale(.84 1.19048)"
            id="lbl1-3-3-0"
            y={55.353378}
            x={269.02347}
            style={{
              lineHeight: 1.25,
              ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              textAlign: 'start',
              fontVariantEastAsian: 'normal',
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="12.3472px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            writingMode="lr-tb"
            textAnchor="start"
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.855373}
          >
            <tspan
              id="tspan1256-09-5-9-5-7-0-9"
              style={{
                ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                textAlign: 'start',
                fontVariantEastAsian: 'normal',
              }}
              y={55.353378}
              x={269.02347}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="12.34722222px"
              fontFamily="Franklin Gothic Medium"
              writingMode="lr-tb"
              textAnchor="start"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.855373}
            >
              {'EST'}
            </tspan>
          </text>
          <path
            id="path1216-5"
            d="M90.112 33.865l16.083 34.476 251.103-.214 17.83-34.49"
            display="inline"
            fill="none"
            stroke="#3fdbe4"
            strokeWidth={1.01983}
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <g id="g336-7-9" transform="matrix(0 2.47939 3.4361 0 109.25 123.889)" display="inline">
            <path
              d="M-17.773 99.38l1.42 1.892h2.177l.338-.78h66.963l.428.57h2.27l1.171-1.559H55.86l-.153.353H-16.37l-.23-.53z"
              id="path3295-6-7-5-3"
              display="inline"
              fill="#168198"
              fillOpacity={1}
              stroke="#3fdce4"
              strokeWidth=".264583px"
              strokeLinecap="butt"
              strokeLinejoin="miter"
              strokeOpacity={1}
            />
          </g>
          <path
            d="M107.726 63.994l47.95-.715"
            id="path8890-2"
            display="inline"
            fill="#fff"
            stroke="#f7f7f7"
            strokeWidth=".933504px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={1}
          />
          <text
            transform="scale(.82458 1.21273)"
            id="lbl1-3-3-5-5"
            y={54.364243}
            x={309.60678}
            style={{
              lineHeight: 1.25,
              ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
              textAlign: 'start',
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="12.3472px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            writingMode="lr-tb"
            textAnchor="start"
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.871368}
          >
            <tspan
              id="tspan1256-09-5-9-5-7-0-1-3"
              style={{
                ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
                textAlign: 'start',
              }}
              y={54.364243}
              x={309.60678}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="12.3472px"
              fontFamily="Franklin Gothic Medium"
              writingMode="lr-tb"
              textAnchor="start"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.871368}
            >
              {'ALM'}
            </tspan>
          </text>
          <text
            transform="scale(.796 1.25628)"
            id="lbl1-3-3-5-4-8"
            y={52.765152}
            x={359.85602}
            style={{
              lineHeight: 1.25,
              ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
              fontVariantLigatures: 'normal',
              fontVariantCaps: 'normal',
              fontVariantNumeric: 'normal',
              fontVariantEastAsian: 'normal',
              textAlign: 'start',
            }}
            xmlSpace="preserve"
            fontStyle="normal"
            fontVariant="normal"
            fontWeight={400}
            fontStretch="normal"
            fontSize="12.3472px"
            fontFamily="Franklin Gothic Medium"
            letterSpacing={0}
            wordSpacing={0}
            writingMode="lr-tb"
            textAnchor="start"
            display="inline"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.902653}
          >
            <tspan
              id="tspan1256-09-5-9-5-7-0-1-9-3"
              style={{
                ///InkscapeFontSpecification: "'Franklin Gothic Medium, Normal'",
                fontVariantLigatures: 'normal',
                fontVariantCaps: 'normal',
                fontVariantNumeric: 'normal',
                fontVariantEastAsian: 'normal',
                textAlign: 'start',
              }}
              y={52.765152}
              x={359.85602}
              fontStyle="normal"
              fontVariant="normal"
              fontWeight={400}
              fontStretch="normal"
              fontSize="12.3472px"
              fontFamily="Franklin Gothic Medium"
              writingMode="lr-tb"
              textAnchor="start"
              fill="#fff"
              fillOpacity={1}
              strokeWidth={0.902653}
            >
              {'MNT'}
            </tspan>
          </text>
          <ellipse
            ry={8.1568441}
            rx={8.4110956}
            cy={44.329205}
            cx={266.13406}
            id="alm_uma"
            display="inline"
            //fill={alm}
            fillOpacity={1}
            fillRule="evenodd"
            stroke="none"
            strokeWidth={0.634991}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="markers stroke fill"
          >
            <animate attributeName="fill" from={led_off} to={alm_uma[0]} dur={alm_uma[1]} repeatCount="indefinite" />
          </ellipse>

          <circle
            transform="matrix(.4119 0 0 .3903 136.893 4.088)"
            r={11.973675}
            cy={94.162521}
            cx={312.73749}
            id="path4259-6-6-4"
            display="inline"
            opacity={0.65}
            fill="#fff"
            fillOpacity={0.996078}
            fillRule="evenodd"
            stroke="none"
            strokeWidth={7.34263}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="markers stroke fill"
            filter="url(#filter4280-8-0)"
          />
          <ellipse
            ry={8.1568441}
            rx={8.4110956}
            cy={44.832851}
            cx={295.99673}
            id="mnt"
            display="inline"
            fill={mnt}
            fillOpacity={1}
            fillRule="evenodd"
            stroke="none"
            strokeWidth={0.634991}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="markers stroke fill"
          />
          <circle
            transform="matrix(.4119 0 0 .3903 166.755 4.591)"
            r={11.973675}
            cy={94.162521}
            cx={312.73749}
            id="path4259-6-2-6"
            display="inline"
            opacity={0.65}
            fill={'#ffffff'}
            fillOpacity={0.996078}
            fillRule="evenodd"
            stroke="none"
            strokeWidth={7.34263}
            strokeLinecap="square"
            strokeLinejoin="round"
            strokeMiterlimit={4}
            strokeDasharray="none"
            strokeOpacity={1}
            paintOrder="markers stroke fill"
            filter="url(#filter4280-5-7)"
          />
        </g>
      </svg>
    </div>
  );
};

const getStyles = stylesFactory(() => {
  return {
    wrapper: css`
      position: relative;
    `,
    svg: css`
      position: absolute;
      top: 0;
      left: 0;
    `,
    textBox: css`
      position: absolute;
      bottom: 0;
      left: 0;
      padding: 10px;
    `,
  };
});
