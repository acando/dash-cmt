import React from 'react';
import { PanelProps } from '@grafana/data';
import { SimpleOptions } from 'types';
import { css, cx } from 'emotion';
import { stylesFactory } from '@grafana/ui';

interface Props extends PanelProps<SimpleOptions> {}

export const SimplePanel: React.FC<Props> = ({ options, data, width, height }) => {
  //const theme = useTheme();

  console.log(data);
  let data_repla: any = data.series.find(({ refId }) => refId === 'UNIDAD')?.name;
  let unidad: any = data_repla.replace(/[.*+?^${}()|[\]\\]/g, '');
  let data_repla2: any = data.series.find(({ refId }) => refId === 'OPCION')?.name;
  let opcion: any = data_repla2.replace(/[.*+?^${}()|[\]\\]/g, '');
  let valor1 = 0;
  let valor2 = 0;
  let valor3 = 0;
  let valor4 = 0;
  let valor = 0;
  console.log(opcion);
  switch (opcion) {
    case '1':
      valor1 = data.series.find(({ name }) => name === 'VALOR')?.fields[1].state?.calcs?.firstNotNull;
      valor2 = data.series.find(({ name }) => name === 'VALOR')?.fields[1].state?.calcs?.lastNotNull;
      if (valor1 === null) {
        valor1 = 0;
      }
      if (valor2 === null) {
        valor2 = 0;
      }
      valor = parseFloat((valor2 - valor1).toFixed(1));
      break;
    case '2':
      valor1 = data.series.find(({ name }) => name === 'VALOR')?.fields[1].state?.calcs?.firstNotNull;
      valor2 = data.series.find(({ name }) => name === 'VALOR')?.fields[1].state?.calcs?.lastNotNull;
      valor3 = data.series.find(({ name }) => name === 'VALOR2')?.fields[1].state?.calcs?.firstNotNull;
      valor4 = data.series.find(({ name }) => name === 'VALOR2')?.fields[1].state?.calcs?.lastNotNull;
      if (valor1 === null) {
        valor1 = 0;
      }
      if (valor2 === null) {
        valor2 = 0;
      }
      if (valor3 === null) {
        valor3 = 0;
      }
      if (valor4 === null) {
        valor4 = 0;
      }
      valor = parseFloat((valor2 - valor1 + (valor4 - valor3)).toFixed(1));

      break;
    case '3':
      valor1 = data.series.find(({ name }) => name === 'VALOR')?.fields[1].state?.calcs?.lastNotNull;
      valor2 = data.series.find(({ name }) => name === 'VALOR2')?.fields[1].state?.calcs?.lastNotNull;
      valor3 = data.series.find(({ name }) => name === 'VALOR3')?.fields[1].state?.calcs?.lastNotNull;
      if (valor1 === null) {
        valor1 = 0;
      }
      if (valor2 === null) {
        valor2 = 0;
      }
      if (valor3 === null) {
        valor3 = 0;
      }
      valor = parseFloat(((valor1 + valor2 + valor3) / 3).toFixed(1));

      break;

    default:
      valor = 0;
      break;
  }

  const styles = getStyles();
  return (
    <div
      className={cx(
        styles.wrapper,
        css`
          width: ${width}px;
          height: ${height}px;
        `
      )}
    >
      <svg
        width={'100%'}
        height={'100'}
        viewBox="0 0 40.999999 25"
        id="svg8"
        //{...props}
      >
        <defs id="defs2">
          <linearGradient id="linearGradient949">
            <stop offset={0} id="stop945" stopColor="#00465f" stopOpacity={1} />
            <stop offset={1} id="stop947" stopColor="#00758d" stopOpacity={1} />
          </linearGradient>
          <linearGradient id="linearGradient933">
            <stop offset={0} id="stop929" stopColor="#0a598c" stopOpacity={1} />
            <stop offset={1} id="stop931" stopColor="#029fbf" stopOpacity={1} />
          </linearGradient>
          <linearGradient
            xlinkHref="#linearGradient933"
            id="linearGradient935"
            x1={0.5608052}
            y1={10.616492}
            x2={32.504219}
            y2={10.616492}
            gradientUnits="userSpaceOnUse"
          />
          <linearGradient
            xlinkHref="#linearGradient949"
            id="linearGradient951"
            x1={16.494652}
            y1={1.9272544}
            x2={16.142069}
            y2={16.727802}
            gradientUnits="userSpaceOnUse"
          />
        </defs>
        <g id="layer1">
          <g
            id="g1004"
            transform="matrix(1.24042 0 0 1.24042 .078 -.633)"
            strokeWidth=".264583px"
            strokeLinecap="butt"
            strokeLinejoin="miter"
            strokeOpacity={0.1}
            fillOpacity={0.3}
          >
            <path
              d="M1.556 20.214l-.863-.809V7.853l.733-.85v-1.28l-.596.34V1.041h4.64v.624l1.549.125.774-.774h23.673l.835.835v4.503l-2.05 2.05v4.105l2.12 2.12v4.698l-.844.845H19.391l-.977-.977H8.187l-.6.601h-2.91l-.544.314z"
              id="path849"
              opacity={1}
              fill="url(#linearGradient935)"
              stroke="#000"
            />
            <path
              d="M2.23 6.466V4.883L4.582 2.53h1.522z"
              id="path851"
              opacity={1}
              fill="#002137"
              fillOpacity={1}
              stroke="#000"
            />
            <path
              d="M2.502 17.215v-4.95l-.733-.732V8.285L8.156 1.9h20.986v10.432l2.23 2.628v.919H18.798l-1.317 1.317z"
              id="path853"
              opacity={1}
              fill="url(#linearGradient951)"
              stroke="#000"
            />
            <path
              d="M5.225 19.222h2.359l.652-.57h9.381v-.379H6.437z"
              id="path855"
              opacity={1}
              fill="#002137"
              fillOpacity={1}
              stroke="#000"
            />
            <path
              d="M18.722 16.977h.983l2.182 2.474h-1.081z"
              id="path857"
              opacity={1}
              fill="#002137"
              fillOpacity={1}
              stroke="#000"
            />
            <path
              d="M20.598 16.977h.983l2.182 2.474h-1.082z"
              id="path857-3"
              opacity={1}
              fill="#002137"
              fillOpacity={1}
              stroke="#000"
            />
            <path
              d="M22.473 16.977h.984l2.182 2.474h-1.082z"
              id="path857-3-6"
              opacity={1}
              fill="#002137"
              fillOpacity={1}
              stroke="#000"
            />
            <path
              d="M24.35 16.977h.982l2.183 2.474h-1.082z"
              id="path857-3-7"
              opacity={1}
              fill="#002137"
              fillOpacity={1}
              stroke="#000"
            />
            <path
              d="M26.225 16.977h.983l2.182 2.474H28.31z"
              id="path857-3-5"
              opacity={1}
              fill="#002137"
              fillOpacity={1}
              stroke="#000"
            />
            <path
              d="M30.354 12.5l.31-.364V8.56l-.27-.156.616-.616v5.455z"
              id="path917"
              opacity={1}
              fill="#029ebe"
              fillOpacity={0.4}
              stroke="none"
            />
            <path
              d="M31.398 13.64l.358-.358V7.548l-.437-.148.999-1.01v8.07z"
              id="path919"
              opacity={1}
              fill="#029bbc"
              fillOpacity={0.4}
              stroke="none"
            />
          </g>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={20.287453}
            y={11.675325}
            id="valor"
            fontStyle="normal"
            fontWeight={400}
            fontSize="7.0015px"
            fontFamily="sans-serif"
            fill="#5df"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.328195}
          >
            <tspan
              id="tspan1006"
              x={20.287455}
              y={11.675325}
              style={{
                textAlign: 'center',
              }}
              fontSize="7.0015px"
              textAnchor="middle"
              fill="#5df"
              strokeWidth={0.328195}
            >
              {valor}
            </tspan>
          </text>
          <text
            xmlSpace="preserve"
            style={{
              lineHeight: 1.25,
            }}
            x={20.884413}
            y={17.356113}
            id="unidades"
            fontStyle="normal"
            fontWeight={400}
            fontSize="5.25112px"
            fontFamily="sans-serif"
            fill="#fff"
            fillOpacity={1}
            stroke="none"
            strokeWidth={0.328195}
          >
            <tspan
              id="tspan1006-5"
              x={20.884413}
              y={17.356113}
              style={{
                textAlign: 'center',
              }}
              fontSize="5.25112px"
              textAnchor="middle"
              fill="#fff"
              strokeWidth={0.328195}
            >
              {unidad}
            </tspan>
          </text>
        </g>
      </svg>
    </div>
  );
};

const getStyles = stylesFactory(() => {
  return {
    wrapper: css`
      position: relative;
    `,
    svg: css`
      position: absolute;
      top: 0;
      left: 0;
    `,
    textBox: css`
      position: absolute;
      bottom: 0;
      left: 0;
      padding: 10px;
    `,
  };
});
