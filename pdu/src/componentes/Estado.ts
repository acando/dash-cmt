import { stylesFactory } from '@grafana/ui';
import { css } from 'emotion';

const getStyles = stylesFactory(() => {
  return {
    alarmaOn: css`
      fill: #f51628;
    `,
    alarmaOff: css`
      fill: gray;
    `,
    botonOn: css`
      fill: #4bee8e;
    `,
    botonOff: css`
      fill: #d10818;
    `,
    rectanguloOn: css`
      fill: #1aea78;
    `,
    rectanguloOff: css`
      fill: gray;
    `,
  };
});

const st = getStyles();

export default st;
