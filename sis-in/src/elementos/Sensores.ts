import { stylesFactory } from '@grafana/ui';
import { css } from 'emotion';

const getStyles = stylesFactory(() => {
  return {
    fallaOn: css`
      fill: #ffd42a;
    `,
    fallaOff: css`
      fill: black;
    `,
    techoOn: css`
      fill: #ff2a2a;
    `,
    techoOff: css`
      fill: #1aea78;
    `,
    sueloOn: css`
      fill: #1aea78;
    `,
    sueloOff: css`
      fill: #00abd6;
    `,
    bocinaOn: css`
      fill: gray;
    `,
    bocinaOff: css`
      fill: #1aea78;
    `,
    fbocinaOn: css`
      fill: red;
    `,
    botonOn: css`
      fill: red;
    `,
    botonOff: css`
      fill: black;
    `,
    fbotonOn: css`
      fill: #ff7203;
    `,
  };
});

const sensor = getStyles();

export default sensor;
