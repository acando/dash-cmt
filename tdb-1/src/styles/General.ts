import { stylesFactory } from '@grafana/ui';
import { css } from 'emotion';

const getStyles = stylesFactory(() => {
  return {
    GeneralON: css`
      fill: red;
    `,
    GeneralOff: css`
      fill: gray;
    `,
  };
});

const stylesGen = getStyles();

export default stylesGen;
