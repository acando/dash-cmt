import { stylesFactory } from '@grafana/ui';
import { css } from 'emotion';

const getStyles = stylesFactory(() => {
  return {
    generalOn: css`
      fill: red;
    `,
    generalOff: css`
      fill: gray;
    `,
  };
});

const almGen = getStyles();

export default almGen;
