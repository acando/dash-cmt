import { stylesFactory } from '@grafana/ui';
import { css } from 'emotion';

const getStyles = stylesFactory(() => {
  return {
    cuadradoOn: css`
      fill: #1aea78;
    `,
    cuadradoff: css`
      fill: gray;
    `,
  };
});

const rectangulo = getStyles();

export default rectangulo;
